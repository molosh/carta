import React, {PureComponent} from 'react';
import '../styles/cost.css'
import CostGraph from "./CostGraph";

//TODO: create function to receive cost from API

class Cost extends PureComponent {
    render() {
        return (
            <div className="cost">
                <div className={"box"}>
                    <h3 className={"title"}>ITEM COST</h3>
                    <h1 className={"price"}>100 $</h1>
                </div>
                <CostGraph />
            </div>
        )
    }
}

export default Cost;