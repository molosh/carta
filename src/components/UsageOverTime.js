import React, {PureComponent} from 'react';
import '../styles/usage-over-time.css';
import {Line} from 'react-chartjs-2'

class UsageOverTime extends PureComponent {
    constructor(props) {
        super(props);
        this.data = {
            labels: ['sep', 'sep', 'sep', 'oct', 'oct', 'oct', 'nov', 'dec', 'jan', 'feb', 'feb', 'mar'],
            datasets: [{
                backgroundColor: '#6b92ab',
                fill: false,
                pointRadius: 2,
                pointBackgroundColor: 'white',
                pointHitRadius: 10,
                pointHoverBackgroundColor: 'white',
                borderColor: '#6b92ab',
                borderWidth: 2.5,
                data: [0.3, 0.2, 0.5, 0.1, 0.2, 0.6, 0.3, 0.3, 0.1, 0.4, 0.5, 0.2, 0.1],
            }]
        };
    }

    render(){
        return (
            <div className={"usage-over-time"}>

                <div className={"graph"}>
                    <h3 className={"title"}>USAGE OVER TIME</h3>
                    <Line
                        data={this.data}
                        options={
                            {
                                responsive: true,
                                maintainAspectRatio: false,

                                layout: {
                                    padding: {
                                        left: 3.5,
                                        right: 10,
                                        top: 11.5,
                                        bottom: 13
                                    }
                                },

                                elements: {
                                    line: {
                                        tension: 0
                                    }
                                },

                                legend: {
                                    display: false
                                },

                                scales: {
                                    yAxes: [{
                                        position: 'top',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Cases',
                                            fontSize: 6,
                                            fontFamily: "SFProText-Light",
                                            fontColor: '#6b92ab',

                                        },
                                        ticks: {
                                            fontSize: 5,
                                            beginAtZero: false,
                                            stepSize: 0.1,
                                            max: 0.6,
                                        }
                                    }],
                                    xAxes: [{
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Date',
                                            fontSize: 6,
                                            fontFamily: "SFProText-Light",
                                            fontColor: '#6b92ab',
                                        },
                                        ticks: {
                                            fontSize: 5,
                                            maxRotation: 0 // angle in degrees
                                        }
                                    }]
                                },

                                tooltips: {
                                    // Disable the on-canvas tooltip
                                    enabled: false,
                                    mode: "point",
                                    intersect: true,
                                    axis: 'x',
                                    position: 'nearest',

                                    custom: function (tooltipModel) {
                                        // Tooltip Element
                                        let tooltipEl = document.getElementById('chartjs-tooltip');

                                        // Create element on first render
                                        if (!tooltipEl) {
                                            tooltipEl = document.createElement('div');
                                            tooltipEl.className = 'tooltip-box';
                                            tooltipEl.id = 'chartjs-tooltip';
                                            tooltipEl.innerHTML = "<div id='cost-tooltip'></div>";
                                            document.body.appendChild(tooltipEl);
                                        }

                                        // Hide if no tooltip
                                        if (tooltipModel.opacity === 0) {
                                            tooltipEl.style.opacity = 0;
                                            return;
                                        }

                                        // Set caret Position
                                        tooltipEl.classList.remove('above', 'below', 'no-transform');
                                        if (tooltipModel.yAlign) {
                                            tooltipEl.classList.add(tooltipModel.yAlign);
                                        } else {
                                            tooltipEl.classList.add('no-transform');
                                        }

                                        function getBody(bodyItem) {
                                            return bodyItem.lines;
                                        }

                                        // Set Text
                                        if (tooltipModel.body) {
                                            let date = '01 / 29 / 17'; // TODO: reformat date.
                                            let count = tooltipModel.dataPoints[0].yLabel;

                                            let innerHtml = '<div>' + count + '<span class="gray"> items were used</span></div>';
                                            innerHtml += '<div><span class="gray">on </span>' + date + '</div>';

                                            let divRoot = tooltipEl.querySelector('#cost-tooltip');
                                            divRoot.innerHTML = innerHtml;
                                        }

                                        // `this` will be the overall tooltip
                                        let position = this._chart.canvas.getBoundingClientRect();
                                        // Display, position, and set styles for font
                                        tooltipEl.style.backgroundColor = 'rgba(235, 239, 243, 0.9)';
                                        tooltipEl.style.borderRadius = '1.5px';
                                        tooltipEl.style.opacity = 1;
                                        tooltipEl.style.position = 'absolute';
                                        tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
                                        tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
                                        tooltipEl.style.fontFamily = "SFProText-Light";
                                        tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                                        tooltipEl.style.paddingLeft = '7.5px';
                                        tooltipEl.style.paddingRight = '7.5px';
                                        tooltipEl.style.paddingTop = '12px';
                                        tooltipEl.style.paddingBottom = '12px';
                                    },
                                },
                            }
                        }
                    />
                </div>
            </div>
        )
    }
}

export default UsageOverTime;