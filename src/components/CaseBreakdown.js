import React, {PureComponent} from 'react';
import '../styles/case-breakdown.css'
import {Bar} from 'react-chartjs-2';

class CaseBreakdown extends PureComponent {
    constructor(props) {
        super(props);
        this.data = {
            labels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
            datasets: [{
                backgroundColor: '#6b92ab',
                data: [4, 2, 1, 2, 2, 3, 5, 1, 3, 2, 0, 2, 1],
            }]

        };
    }

    render() {
        return (
            <div className={"case-breakdown"}>
                <div className="graph">
                    <h3 className={"title"}>CASE BREAKDOWN</h3>
                    <Bar
                        data={this.data}
                        options={
                            {
                                responsive: true,
                                maintainAspectRatio: false,

                                layout: {
                                    padding: {
                                        left: 3.5,
                                        right: 10,
                                        top: 11.5,
                                        bottom: 13
                                    }
                                },

                                title: {
                                    display: true,
                                },

                                scales: {
                                    yAxes: [{
                                        position: 'top',
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Cases',
                                            fontSize: 6,
                                            fontFamily: "SFProText-Light",
                                            fontColor: '#6b92ab',

                                        },
                                        ticks: {
                                            fontSize: 5,
                                            beginAtZero: true,
                                            stepSize: 1,
                                            max: 5,
                                        }
                                    }],
                                    xAxes: [{
                                        gridLines: { display: false },
                                        scaleLabel: {
                                            display: true,
                                            labelString: 'Number of items used',
                                            fontSize: 6,
                                            fontFamily: "SFProText-Light",
                                            fontColor: '#6b92ab',
                                        },
                                        ticks: {
                                            fontSize: 5,
                                            maxRotation: 0 // angle in degrees
                                        }
                                    }]
                                },

                                tooltips: {
                                    // Disable the on-canvas tooltip
                                    enabled: false,
                                    mode: "point",
                                    intersect: true,
                                    axis: 'x',
                                    position: 'nearest',

                                    custom: function (tooltipModel) {
                                        // Tooltip Element
                                        let tooltipEl = document.getElementById('chartjs-tooltip');

                                        // Create element on first render
                                        if (!tooltipEl) {
                                            tooltipEl = document.createElement('div');
                                            tooltipEl.className = 'tooltip-box';
                                            tooltipEl.id = 'chartjs-tooltip';
                                            tooltipEl.innerHTML = "<div id='cost-tooltip'></div>";
                                            document.body.appendChild(tooltipEl);
                                        }

                                        // Hide if no tooltip
                                        if (tooltipModel.opacity === 0) {
                                            tooltipEl.style.opacity = 0;
                                            return;
                                        }

                                        // Set caret Position
                                        tooltipEl.classList.remove('above', 'below', 'no-transform');
                                        if (tooltipModel.yAlign) {
                                            tooltipEl.classList.add(tooltipModel.yAlign);
                                        } else {
                                            tooltipEl.classList.add('no-transform');
                                        }

                                        function getBody(bodyItem) {
                                            return bodyItem.lines;
                                        }

                                        // Set Text
                                        if (tooltipModel.body) {
                                            let countItems = tooltipModel.dataPoints[0].xLabel;
                                            let countCases = tooltipModel.dataPoints[0].yLabel;

                                            let innerHtml = '<div>' + countItems + '<span class="gray"> items were used in</span></div>';
                                            innerHtml += '<div>' + countCases + '<span class="gray"> cases</span></div>';

                                            let divRoot = tooltipEl.querySelector('#cost-tooltip');
                                            divRoot.innerHTML = innerHtml;
                                        }

                                        // `this` will be the overall tooltip
                                        let position = this._chart.canvas.getBoundingClientRect();
                                        // Display, position, and set styles for font
                                        tooltipEl.style.backgroundColor = 'rgba(235, 239, 243, 0.9)';
                                        tooltipEl.style.borderRadius = '1.5px';
                                        tooltipEl.style.opacity = 1;
                                        tooltipEl.style.position = 'absolute';
                                        tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
                                        tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
                                        tooltipEl.style.fontFamily = "SFProText-Light";
                                        tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                                        tooltipEl.style.paddingLeft = '7.5px';
                                        tooltipEl.style.paddingRight = '7.5px';
                                        tooltipEl.style.paddingTop = '12px';
                                        tooltipEl.style.paddingBottom = '12px';
                                    },
                                },
                                legend: {
                                    display: false,
                                }

                            }
                        }
                    />
                </div>
            </div>
        )
    }
}

export default CaseBreakdown;