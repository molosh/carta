import React, {PureComponent} from 'react';
import '../styles/search.css';

class Search extends PureComponent{
    constructor(props){
        super(props);
        this.state = {
            searchValue: ''
        }
    }

    onHandleChangeSearchValue = (e) => {
        this.setState({
            searchValue: e.target.value
        })
    };

    render(){
        return(
            <input type="text" value={this.state.searchValue} onChange={this.onHandleChangeSearchValue}/>
        )
    }
}

export default Search;