import React, {PureComponent} from 'react';
import '../styles/missing.css';
import {Bar} from 'react-chartjs-2'

class MissingGraph extends PureComponent {
    constructor(props) {
        super(props);
        this.data = {
            labels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
            datasets: [{
                backgroundColor: '#6b92ab',
                data: [91, 70, 65, 60, 45, 37, 32, 20, 18, 19, 16, 10, 6],
            }]

        };
    }

    render() {
        return (
            <div className={"graph"}>
                <h3 className={"title"}>NUMBER OF ITEMS MISSING</h3>
                <Bar
                    data={this.data}
                    options={
                        {
                            responsive: true,
                            maintainAspectRatio: false,

                            layout: {
                                padding: {
                                    left: 3.5,
                                    right: 10,
                                    top: 0,
                                    bottom: 13
                                }
                            },

                            title: {
                                display: true,
                            },

                            scales: {
                                yAxes: [{
                                    position: 'top',
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Number of items missing',
                                        fontSize: 6,
                                        fontFamily: "SFProText-Light",
                                        fontColor: '#6b92ab',

                                    },
                                    ticks: {
                                        fontSize: 5,
                                        beginAtZero: true,
                                        stepSize: 20,
                                        max: 100,
                                    }
                                }],
                                xAxes: [{
                                    gridLines: {display: false},
                                    scaleLabel: {
                                        display: false,
                                        labelString: 'Number of items used',
                                        fontSize: 6,
                                        fontFamily: "SFProText-Light",
                                        fontColor: '#6b92ab',
                                    },
                                    ticks: {
                                        display: false,
                                    }
                                }]
                            },

                            tooltips: {
                                // Disable the on-canvas tooltip
                                enabled: false,
                                mode: "point",
                                intersect: true,
                                axis: 'x',
                                position: 'nearest',

                                custom: function (tooltipModel) {
                                    // Tooltip Element
                                    let tooltipEl = document.getElementById('chartjs-tooltip');

                                    // Create element on first render
                                    if (!tooltipEl) {
                                        tooltipEl = document.createElement('div');
                                        tooltipEl.className = 'tooltip-box';
                                        tooltipEl.id = 'chartjs-tooltip';
                                        tooltipEl.innerHTML = "<div id='cost-tooltip'></div>";
                                        document.body.appendChild(tooltipEl);
                                    }

                                    // Hide if no tooltip
                                    if (tooltipModel.opacity === 0) {
                                        tooltipEl.style.opacity = 0;
                                        return;
                                    }

                                    // Set caret Position
                                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                                    if (tooltipModel.yAlign) {
                                        tooltipEl.classList.add(tooltipModel.yAlign);
                                    } else {
                                        tooltipEl.classList.add('no-transform');
                                    }

                                    function getBody(bodyItem) {
                                        return bodyItem.lines;
                                    }

                                    // Set Text
                                    if (tooltipModel.body) {
                                        let countMissingItems = tooltipModel.dataPoints[0].yLabel;

                                        let innerHtml = '<div>';
                                        innerHtml += '<span class="red">' + countMissingItems + '</span>';
                                        innerHtml += '<span class="gray"> missing items in<br>"Name of procedure"</span>';
                                        innerHtml += '<div class="cost">Total cost:<span class="red sb"> 265 $</span></div></div>';

                                        let divRoot = tooltipEl.querySelector('#cost-tooltip');
                                        divRoot.innerHTML = innerHtml;
                                    }

                                    // `this` will be the overall tooltip
                                    let position = this._chart.canvas.getBoundingClientRect();
                                    // Display, position, and set styles for font
                                    tooltipEl.style.backgroundColor = 'rgba(235, 239, 243, 0.9)';
                                    tooltipEl.style.borderRadius = '1.5px';
                                    tooltipEl.style.opacity = 1;
                                    tooltipEl.style.position = 'absolute';
                                    tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
                                    tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
                                    tooltipEl.style.fontFamily = "SFProText-Light";
                                    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                                    tooltipEl.style.paddingLeft = '7.5px';
                                    tooltipEl.style.paddingRight = '7.5px';
                                    tooltipEl.style.paddingTop = '12px';
                                    tooltipEl.style.paddingBottom = '12px';
                                },
                            },
                            legend: {
                                display: false,
                            }

                        }
                    }
                />
            </div>
        )
    }
}

export default MissingGraph;