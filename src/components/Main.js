import React, {PureComponent} from 'react';
import '../styles/main.css';
import Sidebar from './Sidebar';
import Header from './Header';
import Cost from "./Cost";
import Vendor from "./Vendor";
import CaseBreakdown from "./CaseBreakdown";
import UsageOverTime from "./UsageOverTime";
import Surgeon from "./Surgeon";
import Missing from "./Missing";

class Main extends PureComponent {
    render() {
        return (
            <div className={"row"}>
                <Sidebar/>
                <Header/>
                <div className={"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main"}>
                    <div className={"row"}>
                        <div className={"col-sm-6 col-md-6"}>
                            <Cost/>
                            <CaseBreakdown />
                            <Surgeon />
                        </div>
                        <div className={"col-sm-6 col-md-6"}>
                            <Vendor/>
                            <UsageOverTime />
                            <Missing />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


export default Main;