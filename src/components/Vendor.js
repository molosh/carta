import React, {PureComponent} from 'react';
import VendorComparison from './VendorComparison';
import '../styles/vendor.css';

//TODO: create function to receive cost from API

class Vendor extends PureComponent {
    render() {
        return (
            <div className={"vendor"}>
                <div className={"box"}>
                    <h3 className={"title"}>VENDOR</h3>
                    <h1 className={"name"}>TELEFLEX</h1>
                </div>
                <VendorComparison/>
            </div>

        )
    }
}

export default Vendor;