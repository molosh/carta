import React, {PureComponent} from 'react';
import '../styles/cost.css';
import {Line} from 'react-chartjs-2';

class CostGraph extends PureComponent {
    constructor(props) {
        super(props);
        this.data = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(15, 175, 0, 0);
            gradient.addColorStop(0, 'rgba(0, 156, 242, 0.09)');
            gradient.addColorStop(1, '#6b92ab');
            return {
                labels: ['', 'sep', 'sep', 'sep', 'oct', 'oct', 'oct', 'nov', 'dec', 'jan', 'feb', 'feb', 'mar'],
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMax: 250
                        }
                    }]
                },
                datasets: [{
                    label: 'by vendor',
                    backgroundColor: gradient,
                    pointRadius: 0,
                    pointHitRadius: 10,
                    pointHoverBackgroundColor: 'white',
                    borderColor: '#6b92ab',
                    borderWidth: 2.5,
                    data: [25, 37.5, 60, 58.5, 60, 100, 150, 140, 150, 175, 160, 170, 150],
                }]
            }
        };
    }

    render() {
        const data = (canvas) => {
            const ctx = canvas.getContext("2d");
            const gradient = ctx.createLinearGradient(0, 0, 100, 0);
            return {
                fill: true,
                backgroundColor: gradient,
            }
        };

        return (
            <div className={"graph"}>
                <h3 className={"title"}>COST BY TIME</h3>
                <Line
                    data={this.data}
                    options={
                        {
                            maintainAspectRatio: false,

                            scales: {
                                yAxes: [{
                                    position: 'top',
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Cost $',
                                        fontSize: 6,
                                        fontFamily: "SFProText-Light",
                                        fontColor: '#6b92ab',

                                    },
                                    ticks: {
                                        fontSize: 5,
                                        beginAtZero: true,
                                        stepSize: 50,
                                        max: 250,
                                    }
                                }],
                                xAxes: [{
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Date',
                                        fontSize: 6,
                                        fontFamily: "SFProText-Light",
                                        fontColor: '#6b92ab',
                                    },
                                    ticks: {
                                        fontSize: 5,
                                        maxRotation: 0 // angle in degrees
                                    }
                                }]
                            },


                            tooltips: {
                                // Disable the on-canvas tooltip
                                enabled: false,
                                mode: "point",
                                intersect: true,
                                axis: 'x',
                                position: 'nearest',

                                custom: function (tooltipModel) {
                                    // Tooltip Element
                                    let tooltipEl = document.getElementById('chartjs-tooltip');

                                    // Create element on first render
                                    if (!tooltipEl) {
                                        tooltipEl = document.createElement('div');
                                        tooltipEl.className = 'tooltip-box';
                                        tooltipEl.id = 'chartjs-tooltip';
                                        tooltipEl.innerHTML = "<div id='cost-tooltip'></div>";
                                        document.body.appendChild(tooltipEl);
                                    }

                                    // Hide if no tooltip
                                    if (tooltipModel.opacity === 0) {
                                        tooltipEl.style.opacity = 0;
                                        return;
                                    }

                                    // Set caret Position
                                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                                    if (tooltipModel.yAlign) {
                                        tooltipEl.classList.add(tooltipModel.yAlign);
                                    } else {
                                        tooltipEl.classList.add('no-transform');
                                    }

                                    function getBody(bodyItem) {
                                        return bodyItem.lines;
                                    }

                                    // Set Text
                                    if (tooltipModel.body) {
                                        let date = '10 / 28 / 17'; // TODO: reformat date.
                                        let cost = tooltipModel.dataPoints[0].yLabel + " $";

                                        let innerHtml = '<div>' + cost + '<span class="gray"> / item</span></div>';
                                        innerHtml += '<div><span class="gray">on </span>' + date + '</div>';

                                        let divRoot = tooltipEl.querySelector('#cost-tooltip');
                                        divRoot.innerHTML = innerHtml;
                                    }

                                    // `this` will be the overall tooltip
                                    let position = this._chart.canvas.getBoundingClientRect();
                                    // Display, position, and set styles for font
                                    tooltipEl.style.backgroundColor = 'rgba(235, 239, 243, 0.9)';
                                    tooltipEl.style.borderRadius = '1.5px';
                                    tooltipEl.style.opacity = 1;
                                    tooltipEl.style.position = 'absolute';
                                    tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
                                    tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
                                    tooltipEl.style.fontFamily = "SFProText-Light";
                                    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                                    tooltipEl.style.paddingLeft = '7.5px';
                                    tooltipEl.style.paddingRight = '7.5px';
                                    tooltipEl.style.paddingTop = '12px';
                                    tooltipEl.style.paddingBottom = '12px';
                                },
                            },
                            legend: {
                                display: true,
                                labels: {
                                    fontSize: 6,
                                    usePointStyle: true,
                                    fontColor: '#d65c3d'
                                }
                            }
                        }
                    }
                />
            </div>
        )
    }
}

export default CostGraph;