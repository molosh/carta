import React, {PureComponent} from 'react';
import '../styles/surgeon.css'

class Surgeon extends PureComponent {
    render(){
        return (
            <div className={"surgeon"}>
                <h3 className={"title"}>USAGE BY A SURGEON</h3>
                <div className={"graph"}></div>
            </div>
        )
    }
}

export default Surgeon;