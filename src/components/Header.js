import React, {PureComponent} from 'react';
import '../styles/header.css';


class Header extends PureComponent {
    render() {
        return (
            <header className={"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 item-details"}>
                <h1 className={"title"}>ITEM DETAILS</h1>
                <div className={"content"}>
                    <h3 className={"item-name"}>CATH ERCP BLK 200CM 3.5FR #G214521#100019</h3>
                    <h3 className={"item-id"}>ID: 312036709700</h3>
                </div>
            </header>
        );
    }
}

export default Header;
