import React, {PureComponent} from 'react';
import '../styles/missing.css'
import MissingGraph from "./MissingGraph";

//TODO: create function to receive cost from API

class Missing extends PureComponent {
    render() {
        return (
            <div className="missing">
                <div className={"box"}>
                    <h3 className={"title"}>ITEM MISSING</h3>
                    <h1 className={"price"}>500 $</h1>
                </div>
                <MissingGraph />
            </div>
        )
    }
}

export default Missing;