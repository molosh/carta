import React, {PureComponent} from 'react';
import bar_chart from '../assets/img/icons/bar-chart.svg';
import '../styles/sidebar.css';


class Sidebar extends PureComponent {
    render() {
        return (
            <div className={"col-sm-3 col-md-2 sidebar"}>
                <div className={"logo-title-wrap"}>
                    <a href="/" className={"logo-link"}>
                        <img src={bar_chart} alt={"bar_chart"} width="41.8px" height="41.8px"/>
                        <div className={"logo-title"}>CARTA</div>
                    </a>
                </div>

                <ul className="main-menu">
                    <li>
                        <a href="#">
                            <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                            Patient Explorer
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                            Carta Apps
                        </a>
                        <ul className={"submenu"}>
                            <li>
                                <a href="#">
                                    <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                                    Cohort Designer
                                </a>
                            </li>
                            <li>

                                <a href="#">
                                    <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                                    Surgical Supplies
                                </a>
                                <ul className={"submenu"}>
                                    <li><a href="#">Item table</a></li>
                                    <li><a href="#">Case table</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                                    Resource Analyzer
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src={bar_chart} alt={"bar_chart"} width="24px" height="24px"/>
                                    Flow Manager
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Sidebar;
