import React, {PureComponent} from 'react';
import {Switch, Route} from 'react-router-dom';
import Main from './components/Main';


class App extends PureComponent {
    render() {
        return (
            <div className={"container-fluid "}>
                <Switch>
                    <Route exact path='/' component={Main} />
                </Switch>
            </div>
        );
    }
}

export default App;
